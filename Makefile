POLARIS_DATADIR=`pwd`/data

build: Dockerfile polaris_wrapper.sh
	docker build \
		--build-arg USER_ID=`id -u ${USER}` \
		--build-arg GROUP_ID=`id -g ${USER}` \
		. \
		-t polaris-ml:latest

$(POLARIS_DATADIR):
	mkdir $(POLARIS_DATADIR) || true

shell:
	docker run \
		-v $(POLARIS_DATADIR):/tmp/polaris:Z \
		-it \
		polaris-ml:latest \
		/bin/sh

EXAMPLE_SAT=LightSail-2
EXAMPLE_CACHEDIR=/tmp/polaris/LightSail-2/cache
EXAMPLE_STARTDATE=2019-07-21T20:00:00
EXAMPLE_ENDDATE=2019-07-21T21:00:00
example_fetch: $(POLARIS_DATADIR)
	docker run \
		-v $(POLARIS_DATADIR):/tmp/polaris:Z \
		polaris-ml:latest \
		polaris fetch \
			-s $(EXAMPLE_STARTDATE
			-e $(EXAMPLE_ENDDATE) \
			--cache_dir $(EXAMPLE_CACHEDIR) \
			$(EXAMPLE_SAT)

batch: $(POLARIS_DATADIR)
	docker run \
		-v $(POLARIS_DATADIR):/tmp/polaris:Z \
		polaris-ml:latest \
		polaris batch --config_file /tmp/polaris/polaris_config.json
