FROM python:3.7

ARG USER_ID=1000
ARG GROUP_ID=1000

RUN groupadd --gid ${GROUP_ID} polaris && \
	useradd --no-log-init --uid ${USER_ID} --gid polaris -s /bin/bash polaris

RUN pip install polaris-ml
ADD polaris_wrapper.sh /polaris_wrapper.sh
USER polaris
WORKDIR /tmp
CMD ["/polaris_wrapper.sh"]
