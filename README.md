# polaris-docker

A Docker container for [Polaris](https://gitlab.com/crespum/polaris).

At the moment, the main use case for this is automated updated of the
Polaris demo site, https://deepchaos.space.  However, it should
definitely be possible to use this for other work, including
day-to-day work with Polaris.

The container is configured to run Polaris as a non-privileged user,
rather than as root.  The Makefile will set the UID and GID of that
non-privileged user to match that of the user running `make`, but this
can be adjusted at build time; see below for details.

The Makefile is configured to create a directory within this repo
named `data` which will hold Polaris cache files.  If you need to
modify this, have a look at the use of the `POLARIS_DATADIR` Makefile
variable.


## Building the container

Just run:

```
make build
```

This will build the container so that the artifacts it produces
(cache, graph files, etc) will be owned by you (the user running the
`make` command).

If you need to specify the UID and GID, have a look at the `build:`
target of the Makefile, and the `build-arg` section in particular.

## Running `polaris batch`

- Copy the polaris config file to the data directory:

```
cp /path/to/polaris_config.json ./data/
```

- Run `make batch`:

```
make batch
```

Output will be in the `./data` directory.

## Running ad-hoc Polaris commands

The Makefile will run Docker so that the data volume is mounted within
the container at `/tmp`; this matches the Polaris default for its
cache directory.

The Makefile contains example commands for Polaris commands.  If you
want to run these commands without `make`, the format is:

```
docker run \
	-v [data directory]:/tmp/polaris:Z \
	polaris-ml:latest
	[polaris command and its arguments]
```

If `[polaris command]` starts with `polaris`, that first word will be
ignored.  Thus, both of these commands will do the right thing:

- `docker run polaris-ml:latest polaris fetch [...]`
- `docker run polaris-ml:latest fetch [...]`

(The volume arguments have been left out for clarity.)
